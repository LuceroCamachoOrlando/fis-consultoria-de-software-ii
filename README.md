# Reportes - Consultoría de software ii

## Nearsoft
![Logo Nearsoft](imagenes/nearsoft-aec.svg)
### Link
[Nearsoft.com](https://nearsoft.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Nearsoft+Inc.+DF/@19.4130209,-99.1644572,15z/data=!4m5!3m4!1s0x0:0x1ffdbfbfc6116db5!8m2!3d19.4130406!4d-99.1644615?hl=es-419)
### Acerca de
Nearsoft tiene que ver con el desarrollo de software. 
Nuestros equipos se autogestionan y entregan a tiempo. 
Tratamos nuestro trabajo como un oficio y aprendemos unos de otros. 
Constantemente subimos el listón.
### Servicios:
- Desarrollo web.
- Desarrollo de productos.
- Desarrollo de aplicaciones móviles
- Desarrollo IoT.
- Desarrollo de software en general.
### Presencia
[Facebook](https://www.facebook.com/Nearsoft/)
[Twitter](https://twitter.com/Nearsoft)
### Ofertas laborales
[LinkedIn](https://www.linkedin.com/company/nearsoft/)
### Blog
[Blog](https://nearsoft.com/blog/)
### Tecnologías
- Kotlin
- Swift
- Python
- JavaScript
- HTML5
- CSS3

## Tiempo Development
![Logo TiempoDev](imagenes/tiempo-development-logo.png)
### Link
[tiempo Development](https://www.tiempodev.com/)
### Ubicación
[Monterrey](https://www.google.com/maps/place/Tiempo+Development+Monterrey/@20.6779413,-108.7097206,5z/data=!4m9!1m2!2m1!1sTiempo+Development!3m5!1s0x8662bfbbe1227fd7:0xfa04fc0360c31a08!8m2!3d25.646523!4d-100.2916083!15sChJUaWVtcG8gRGV2ZWxvcG1lbnQiA4gBAZIBEGNvcnBvcmF0ZV9vZmZpY2U?hl=es-419)
[Guadalajara](https://www.google.com/maps/place/Tiempo+Development+Guadalajara/@20.6779413,-108.7097206,5z/data=!4m9!1m2!2m1!1sTiempo+Development!3m5!1s0x8428ae1301d60c27:0x8a24bec5ecd5776!8m2!3d20.6778917!4d-103.3791929!15sChJUaWVtcG8gRGV2ZWxvcG1lbnQiA4gBAZIBEHNvZnR3YXJlX2NvbXBhbnk?hl=es-419)
[Hermosillo](https://www.google.com/maps/place/Tiempo+Development/@17.6481329,-111.4782753,5z/data=!4m9!1m2!2m1!1sTiempo+Development!3m5!1s0x86ce83dfea74768b:0xed255fa0e03378fc!8m2!3d29.0960969!4d-111.0234822!15sChJUaWVtcG8gRGV2ZWxvcG1lbnQiA4gBAZIBEHNvZnR3YXJlX2NvbXBhbnk?hl=es-419)
### Acerca de
Equipos de alto rendimiento distribuidos globalmente
Tiempo es ampliamente reconocida como una de las principales empresas de ingeniería de software de EE. UU. 
Poniendo a trabajar nuestros recursos de ingeniería independientes de la costa y equipos de alto 
rendimiento con un enfoque incansable en los resultados del cliente, Tiempo diseña, construye e 
implementa software que deleita a los clientes.
### Servicios
**Plan**
- Análisis técnico
- Arquitectura de software
- Diseño UI/UX
- Product management
**Maintain**
- Soporte y mantenimiento de aplicaciones
- Tests de calidad 
**Build**
- Desarrollo de aplicaciones web
- Desarrollo de aplicaciones móviles
- Integración con Apps/API
**Enhance**
- Data Science
**Modernize**
- IoT
- Microservicios
- DevOps
- Tests de automatización
- Cloud Native
**Pre-packaged solutions sets**
- Cloud Cost Management
- Data Science
- Cloud Migration Services
### Presencia
[Twitter](https://twitter.com/TiempoSoftware)
### Ofertas laborales
[LinkedIn](https://mx.linkedin.com/company/tiempo-development)
### Blog
[Blog](https://twitter.com/TiempoSoftware)
### Tecnologías
- Java
- Kotlin
- Objective-C
- Xamarin
- Swift
- Adobe PhoneGap
- React
- CSS3
- HTML5
- JavaScript
- Angular.JS
- Microsoft.NET
- Python
- Flask
- Django
- Node.js
- Express
- PHP
- Laravel
- C
- AWS
- Azure
- Google CLoud
- LAMP
- Falcon
- Tornado
- Kafka
- Kubernetes
- Spring boot
- Docker
- Git
- Trello
- MySQL

## michelada.io
![Logo michelada.io](imagenes/michelada-io.png)
### Link
[michelada.io](https://www.michelada.io/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/michelada.io/@19.266788,-103.717539,15z/data=!4m5!3m4!1s0x0:0x3dbaa3579153065f!8m2!3d19.266788!4d-103.717539?hl=es-419)
### Acerca de
Planificamos, codificamos y lanzamos increíbles productos web y móviles.
### Servicios 
- Desarrollo web
- Desarrollo móvil
- Incrementar equipos
- Mejorar productos
### Presencia
[Facebook](https://www.facebook.com/micheladaio)
[Twitter](https://twitter.com/micheladaio)
[GitHub](https://github.com/michelada)
### Ofertas laborales
[Vacantes](https://jobs.michelada.io/)
### Blog
[Blog](https://twitter.com/micheladaio)
### Tecnologías
- Ruby on Rails
- Kotlin
- Swift
- JavaScript
- CSS3
- HTML5

## MagmaLabs
![Logo MagmaLabs](imagenes/magmalabs.png)
### Link
[magmalabs.io](https://www.magmalabs.io/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/MagmaLabs/@19.2660316,-103.7129614,16.5z/data=!4m12!1m6!3m5!1s0x0:0x49107047fc2c8c23!2sMagmaLabs!8m2!3d19.2652118!4d-103.7107613!3m4!1s0x0:0x49107047fc2c8c23!8m2!3d19.2652118!4d-103.7107613)
### Acerca de
Nuestro trabajo
Somos una agencia de diseño web de servicio completo y desarrollo de software personalizado, que incluye departamentos de marketing y producción de medios. Podemos asumir proyectos completos de principio a fin, o simplemente aumentar su equipo existente.

Nuestras capacidades de diseño incluyen Product Design Sprints (PDS), para ayudarlo a determinar el producto exacto que debe entregar a su mercado.

Empleamos a varios comprometidos principales con importantes proyectos de código abierto. Las comunidades de apoyo interno muy unidas garantizan que sus consultores de Magma estén siempre actualizados sobre las mejores prácticas más recientes en un amplio espectro de áreas de tecnología.

Lean Startup corre en nuestro torrente sanguíneo. Además de desarrollar nuestros propios productos internos, tenemos experiencia práctica y probada en ayudar a los clientes a determinar cuál debería ser su Producto Mínimo Viable (MVP).

¡No solo trabajamos con startups! Algunas de las aplicaciones cliente que hemos creado sirven a decenas de millones de usuarios por día o más, y nuestra gente las mantiene en funcionamiento.
### Servicios 
- Software Product Design
- Desarrollo web
- Soluciones de software
### Presencia
[Facebook](https://www.facebook.com/magmalabsio)
[Twitter](https://twitter.com/weareMagmaLabs)
[Instagram](https://www.instagram.com/magmalabs)
### Ofertas laborales
[LinkedIn](https://www.linkedin.com/company/magmalabs)
[Ofertas laborales](https://magmalabs.bamboohr.com/jobs/)
### Blog
[Blog](http://blog.magmalabs.io/)
### Tecnologías
- Ruby on Rails
- React
- React Native

## IcaliaLabs
![Logo IcaliaLabs](imagenes/icalialabs.png)
### Link
[icalialabs](https://www.icalialabs.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Av.+Eugenio+Garza+Sada+3820,+Roma,+64780+Monterrey,+N.L./@25.6339214,-100.2845055,17z/data=!3m1!4b1!4m5!3m4!1s0x8662bf9f7c340e07:0x901f5932e793fa6d!8m2!3d25.6339214!4d-100.2823168)
### Acerca de
En Icalia Labs transformamos negocios con soluciones digitales poderosas y adaptables que satisfacen las necesidades de hoy y desbloquean las oportunidades del mañana.
### Servicios 
- Ruby on Rails and Staff Augmentation Services
- Transformación digital
- Design Sprint and Workshops
- Custom Software Development Services
### Presencia
[Dribble](https://dribbble.com/icalialabs/members)
[GitHub](https://github.com/IcaliaLabs)
### Ofertas laborales
[LinkedIn](https://www.linkedin.com/company/icalia-labs/)
[Clutch.co](https://clutch.co/profile/icalia-labs#summary)
### Blog
[Blog](https://www.icalialabs.com/blog-software-development-tips-and-news)
### Tecnologías
- Ruby 
- Ruby on Rails
- Python
- ReactJS
- EmberJS
- GraphQL
- React Native
- Bootstrap
- HTML
- CSS
- JS
- Kotlin
- Swift

## iTexico
![Logo michelada.io](imagenes/itexico.png)
### Link
[iTexico](https://www.itexico.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Improving/@20.6899988,-103.4162525,18.25z/data=!3m1!5s0x8428aeee0bfb0ac7:0xbf1155c67199fe4c!4m12!1m6!3m5!1s0x8428add6c3c1323f:0x1b33ebadfe7f1b2f!2sImproving!8m2!3d20.6898348!4d-103.4155984!3m4!1s0x8428add6c3c1323f:0x1b33ebadfe7f1b2f!8m2!3d20.6898348!4d-103.4155984)
### Acerca de
Impulsamos la innovación digital que permite a las empresas transformar su negocio a través de una amplia gama de ofertas de servicios digitales que incluyen: diseño, ingeniería de productos, control de calidad, dispositivos móviles, nube e inteligencia artificial.
### Servicios 
- UI/UX Design
- Software Development Services
- QA
- Moible Software Development
- CLoud
- A.I.
- .NET
- Desktop Development
- DevOps
### Presencia
[Facebook](https://www.facebook.com/itexico)
[Twitter](https://twitter.com/improvingmx)
[YouTube](https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw)
### Ofertas laborales
[LinkedIn](https://www.linkedin.com/company/795788)
[Solicitudes](https://www.itexico.com/careers)
### Blog
[Blog](https://www.itexico.com/blog)
### Tecnologías
- JavaScript
- Java
- Kotlin
- .NET
- Spring
- Flutter

## Platzi
![Logo Platzi](imagenes/Platzi.jpg)
### Link
[Platzi](https://platzi.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/michelada.io/@19.266788,-103.717539,15z/data=!4m5!3m4!1s0x0:0x3dbaa3579153065f!8m2!3d19.266788!4d-103.717539?hl=es-419)
### Acerca de
Platzi es una plataforma latinoamericana de educación en línea. ​ Fue fundada en 2014 por el ingeniero colombiano Freddy Vega y por el informático guatemalteco Christian Van Der Henst.
### Servicios 
- Cursos online
### Presencia
[Facebook](https://facebook.com/platzi)
[Twitter](https://twitter.com/platzi)
[Instagram](https://www.instagram.com/platzi/)
[YouTube](https://www.youtube.com/channel/UC55-mxUj5Nj3niXFReG44OQ)
[TikTok](https://tiktok.com/@aprendeconplatzi)
[Spotify](https://open.spotify.com/show/66phcUoQsM3URyzhFDz9ig)
### Ofertas laborales
[LinkedIn](https://linkedin.com/school/platzi-inc)
### Blog
[Platzi blog](https://platzi.com/blog/)
### Tecnologías
- JavaScrip
- HTML
- CSS
- Kotlin
- Swift

## bitso
![Logo bitso](imagenes/Bitso-dark.png)
### Link
[bitso](https://bitso.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Oficinas+bitso/@19.4295734,-99.2052136,17z/data=!3m1!4b1!4m5!3m4!1s0x85d201f7d9e50377:0xe421fec07501d537!8m2!3d19.4295734!4d-99.2030249)
### Acerca de
Bitso es una plataforma para comprar, vender y usar criptomonedas, con operaciones en Argentina, Brasil y México. Fundada en 2014 por Ben Peters y Pablo González, con la incorporación de Daniel Vogel en 2015 como socio igualitario.
### Servicios 
- Compra y venta de criptomonedas, criptodivizas y criptocurrency en general
### Presencia
[Facebook](https://www.facebook.com/bitsoex)
[Twitter](https://twitter.com/bitso)
[Instagram](https://instagram.com/bitso)
[YouTube](https://www.youtube.com/bitso)
[Telegram](https://t.me/Bitso_Mexico)
### Ofertas laborales
[Trabajos](https://bitso.com/jobs)
### Blog
[Blog](https://blog.bitso.com/)
### Tecnologías
- Kotlin
- Java
- Scrum
- JavaScript
- FLutter
- Swift
- Android
- IOS

## GrowIT
![Logo GrowIT](imagenes/growit.png)
### Link
[GrowIT](https://www.grw.com.mx/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/N%C3%A1poles,+03810+Ciudad+de+M%C3%A9xico,+CDMX/@19.3936175,-99.1847367,15z/data=!4m8!1m2!2m1!1sSan+Antonio+120+Int.+101+y+102+Col.+Napoles,+Delegaci%C3%B3n+Benito+Juarez+CDMX,+C.P.+03810!3m4!1s0x85d1ff70ff978bff:0x349ea20cc7ab37b7!8m2!3d19.3943574!4d-99.1774462)
### Acerca de
Grow IT es una empresa de desarrollo de software especializada en productos de Microsoft Dynamics. 

Ayudamos a los partners a reducir la brecha entre las capacidades del producto y las necesidades de sus clientes utilizando metodologías de desarrollo de software para planear y controlar el ciclo de desarrollo, lo que asegura la calidad del producto final optimizando la inversión.
### Servicios 
- NOM2001 D365 Conector
- Cobro de tarjeta MIT en POS
- Repositorios de clientes digitales
- Facturación electrónica para Dynamics SL
- Sigein Conector
### Presencia
[Facebook](https://www.facebook.com/growitc)
[Instagram](https://www.instagram.com/grow_it_mx)
### Ofertas laborales
[LinkedIn](https://www.linkedin.com/company/grow-it-consulting)
[Bolsa de trabajo](https://www.grw.com.mx/bolsa-de-trabajo/)
### Blog
[Blog](https://www.grw.com.mx/blog/)
### Tecnologías
- .NET
- POO
- Scrum

## contpaqi
![Logo michelada.io](imagenes/CONTPAQi.png)
### Link
[contpaqi](https://www.contpaqi.com/)
### Ubicación
[Ubicación](https://www.google.com/search?tbs=lf:1,lf_ui:4&tbm=lcl&sxsrf=AOaemvL0tXPNxmrFj67xdvIYP0UyAosXYA:1634180929893&q=contpaqi&rflfq=1&num=10&ved=2ahUKEwico8yu9sjzAhVT_J4KHRdKBOAQtgN6BAgKEAQ#rlfi=hd:;si:14952358655611393936;mv:[[19.176002308711503,-96.08430934725374],[19.11478439117776,-96.12825465975374],null,[19.145396188418637,-96.10628200350374],14])
### Acerca de
SOBRE NOSOTROS
En CONTPAQi® somos pioneros en software contable y administrativo. Nuestro objetivo es impulsar a las empresas y profesionales a lo largo de México.
### Servicios 
- Procesos contables
- Procesos comerciales
- Productividad
- Nube
- WOPEN
### Presencia
[Facebook](https://www.facebook.com/CONTPAQi)
[Twitter](https://twitter.com/CONTPAQi)
[YouTube](https://www.youtube.com/contpaqi1)
### Ofertas laborales
[Bolsa de trabajo](https://www.linkedin.com/company/contpaqi1)
### Blog
[Blog](https://blog.contpaqi.com/)
### Tecnologías
- Azure
- JavaScript
- CSS
- HTML
- JAVA
- MySQL

## EDENRED
![Logo edenred](imagenes/Edenred.jpg)
### Link
[Edenred](https://www.edenred.mx/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Edenred+M%C3%A9xico+S.A.+De+C.V./@19.439292,-99.192705,16z/data=!4m9!1m2!2m1!1sedenred!3m5!1s0x85d1f8a9d42d3731:0x18a22f0a850580ad!8m2!3d19.4391778!4d-99.1897505!15sCgdlZGVucmVkkgEVZmluYW5jaWFsX2luc3RpdHV0aW9u)
### Acerca de
Edenred es una plataforma líder de pagos y servicios, el compañero diario de las personas en el trabajo en 46 países. El Grupo Edenred cree firmemente en el crecimiento, es por ello que se esfuerza por ser un líder digital responsable y comprometido con ayudar a los trabajadores, empresas, comercios, autoridades públicas y comunidades locales.

Conectando a 50 millones de usuarios y 2 millones de socios comerciales a través de más de 850,000 clientes corporativos.
### Servicios 
- Teletrabajo
### Presencia
[Facebook](https://www.facebook.com/EdenredMx)
[Twitter](https://twitter.com/edenredmexico)
[Instagram](https://www.instagram.com/edenred_mx/)
[YouTube](https://www.youtube.com/user/EdenredMX)
### Ofertas laborales
[LinkedIn](https://www.linkedin.com/company/edenred-m-xico/)
### Blog
[Blog](https://blog.edenred.mx/)
### Tecnologías
- JavaScript
- PHP
- HTML
- CSS

## Ordico
![Logo michelada.io](imagenes/ordico.png)
### Link
[ordico](https://www.ordico.mx/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Ordico/@16.7499576,-93.1279258,18.25z/data=!4m5!3m4!1s0x0:0x8894e1e5866df9f7!8m2!3d16.7499512!4d-93.1272781)
### Acerca de
### Servicios 
- Finanzas
- Retail
- Transporte
- Manufactura
- Logistica
- E-Commerce
- Construcción
- Distribución
### Presencia
[Facebook](https://www.ordico.mx/clkn/https/www.facebook.com/Ordicomx)
### Ofertas laborales
[Correo](hramos@ordico.mx)
### Blog
[Blog](https://blog.bitso.com/)
### Tecnologías
- Scrum
- Java
- JavaScript

## Abraxas Intelligence
![Logo Abraxas](imagenes/abraxas.png)
### Link
[Abraxas Intelligence](https://www.abraxasintelligence.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/ABRAXAS+Biosystems/@19.3387883,-99.1973317,17z/data=!4m5!3m4!1s0x85d20000d5d29a4d:0x4b7a3dd5bdb27fdf!8m2!3d19.3387895!4d-99.1951562)
### Acerca de
Brindamos aprendizaje automático, soluciones de inteligencia artificial y productos de ciencia de datos para mejorar los procesos, aprovechar las oportunidades comerciales y agregar un valor significativo a las organizaciones.
### Servicios 
- A.I.
- Productos de Ciencia de Datos
### Presencia
[GitHub](https://github.com/Grupo-Abraxas)
### Ofertas laborales
[Trabajos](https://www.abraxas.ventures/careers)
[LinkedIn](https://www.linkedin.com/company/abraxas-group/)
### Blog
[Blog](https://blog.bitso.com/)
### Tecnologías
- Python

## Tango
![Logo Tango Source](imagenes/tangosource.png)
### Link
[Tango.io](https://tango.io/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Tango/@19.2649091,-103.7156356,17z/data=!4m5!3m4!1s0x84255ac1302c3677:0x9de571050b13255a!8m2!3d19.2649472!4d-103.7134232)
### Acerca de
Equipos impulsados ​​culturalmente con pasión por el producto
Tango (anteriormente TangoSource), ayuda a empresas emergentes, medianas y empresariales innovadoras a desarrollar productos digitales impactantes a través de asociaciones apasionadas. ¡Hemos ayudado a más de 100 empresas a desarrollar y escalar sus productos de software desde 2009! "
### Servicios 
- Minimum viable product
- Staff augmentation
- Technical debt resolution
- Product strategy
- Product scalability and acceleration
### Presencia
[Facebook](https://www.facebook.com/GoTango.io/)
[Twitter](https://twitter.com/tango_io)
[Instagram](https://www.instagram.com/tangodotio/)
### Ofertas laborales
[Trabajos](https://jobs.lever.co/tango)
[LinkedIn](https://www.linkedin.com/company/tango-io/)
### Blog
[Blog](https://blog.tango.io/)
### Tecnologías
- Python
- NodeJS
- Ruby on Rails
- AngularJS
- Bootstrap
- SCSS
- HAML

## BBVA
![Logo BBVA](imagenes/bbva.png)
### Link
[BBVA](https://www.bbva.mx/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Atm%2FCajero+BBVA+Suc+Oaxaca+Principal90/@17.0695395,-96.7275913,15z/data=!4m9!1m2!2m1!1sbbva!3m5!1s0x85c7223f09d6be81:0x8ae233ca9e44dba0!8m2!3d17.0628076!4d-96.7253367!15sCgRiYnZhIgOIAQGSAQRiYW5r?hl=es-419)
### Acerca de
BBVA México es una filial bancaria mexicana, propiedad de BBVA, fundada en el año 1932 como Banco de Comercio. Desde el año 2000, Bancomer es adquirido por BBVA.​En 2019, BBVA inicia un proceso de unificación de su nombre en todo el mundo y en 2020 se elimina la marca "Bancomer" para llamarse solamente BBVA.
### Servicios 
- Banco
### Presencia
[Facebook](https://bbva.info/2MDG5Z8)
[Twitter](https://bbva.info/2J1E9F1)
[Instagram](https://bbva.info/2ZzFAB1)
[YouTube](https://bbva.info/2XGWtwm)
### Ofertas laborales
[LinkedIn](https://bbva.info/2Yq8O5t)
### Blog
[Blog](https://blog.bitso.com/)
### Tecnologías

## michelada.io
![Logo michelada.io](imagenes/michelada-io.png)
### Link
[michelada.io](https://www.michelada.io/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/michelada.io/@19.266788,-103.717539,15z/data=!4m5!3m4!1s0x0:0x3dbaa3579153065f!8m2!3d19.266788!4d-103.717539?hl=es-419)
### Acerca de
### Servicios 
### Presencia
[Facebook](https://www.facebook.com/magmalabsio)
[Twitter](https://twitter.com/weareMagmaLabs)
[Instagram](https://www.instagram.com/magmalabs)
### Ofertas laborales
[Trabajos](https://clutch.co/profile/icalia-labs#summary)
### Blog
[Blog](https://www.bbva.com/es/mx/)
### Tecnologías
- Kotlin
- Swift
- HTML
- CSS
- JavaScript
